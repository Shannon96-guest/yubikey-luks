Source: yubikey-luks
Section: admin
Priority: optional
Maintainer: Yubikey-LUKS Maintainers <yubikey-luks@tracker.debian.org>
Uploaders:
	Markus Frosch <lazyfrosch@debian.org>,
	Nicolas Braud-Santoni <nicolas@braud-santoni.eu>
Build-Depends: debhelper (>= 11), dh-exec
Standards-Version: 4.1.4
Homepage: https://github.com/cornelinux/yubikey-luks
Vcs-Browser: https://salsa.debian.org/auth-team/yubikey-luks
Vcs-Git: https://salsa.debian.org/auth-team/yubikey-luks.git

Package: yubikey-luks
Architecture: all
Depends: cryptsetup, initramfs-tools, yubikey-personalization (>= 1.5), ${misc:Depends}
Description: YubiKey two factor authentication for LUKS disks
 With this extension to the initramfs-tools, you can unlock a LUKS encrypted
 disk using your YubiKey as a second factor.
 .
 The challenge-response mechanism of the YubiKey is used to generate a response
 based on a PIN/password you have to enter.
 .
 Only the combination of the correct password and the matching YubiKey will
 generate a response, that is a valid key of the LUKS disk. Alternatively
 you can use any other LUKS passphrase when the YubiKey is not present.
